#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "task.h"
/*
This program is a short to-do list application that sorts tasks based on due date and priority.

A short list of tasks:
add/remove/edit name - adds/removes/edits new task with that name
list - shows all tasks
menu - goes into menu (not yet implemented)
*/

int main(int argc, char** argv){
	//std::vector<Task> oldTasks;
	//std::vector<Task> tasks;
	if(argc > 1){
		if(std::string(argv[1]) == "-a"){
		std::fstream rewriter;
		rewriter.open("tasklist.dat", std::fstream::in | std::fstream::app);
		int a,b;
		bool c1;
		std::string c,d,e;
		std::cout<<"For month and day, please write in numerical form (e.g. 10/15)\nWhen is this due?\nMonth? ";
		std::cin>>a;
		std::cout<<"\nDay? ";
		std::cin>>b;
		std::cout<<"\nIs this urgent? ";
		std::cin>>c;
		if(c == "y" || c == "yes") c1 = true;
		if(c == "n" || c == "no") c1 = false;
		std::cout<<"\nWhat is this for? ";
		std::cin>>d;
		std::cout<<"\nDescription? ";
		std::cin.ignore();
		getline(std::cin, e);
		std::cout<<"\n";

		Task newTask (std::string(argv[2]),e,b,a,d,c1);
		std::cout<<"\n\n\nTask made!\n\n\n";
		newTask.print();
		rewriter<<std::string(argv[2])<<"\nDue On: "<<a<<"/"<<b<<"\nFor: "<<d<<"\nDescription: "<<e<<"\n";
		//tasks.push_back(newTask);
		rewriter.close();

		}

		else if (std::string(argv[1]) == "-r"){
		bool deleted = false;
		std::fstream reader;
		std::fstream writer;
		std::string line;
		reader.open("tasklist.dat", std::fstream::in);
		writer.open("newtasklist.dat", std::fstream::in | std::fstream::out | std::fstream::trunc);
		while(getline(reader,line)){
			if(line == std::string(argv[2])){
				getline(reader, line);
				getline(reader, line);
				getline(reader, line);
				getline(reader, line);
			}
			if(line == ""){
				writer<<line;
			}
			else{
				writer<<line<<"\n";
			}
		}
		reader.close();
		writer.close();
		remove("tasklist.dat");
		rename("newtasklist.dat", "tasklist.dat");
		}

		else if(std::string(argv[1]) == "-e"){
			std::cout<<"Not implemented\n";
		}

		else if (std::string(argv[1]) == "-l"){
		std::fstream reader;
		reader.open("tasklist.dat", std::fstream::in);
		std::string line;
		while(getline(reader,line)){
			std::cout<<line<<"\n";
		}
		reader.close();

		}

		else{
			std::cout<<"Error 01: Invalid Command\n";
		}
	}
	else{
		std::cout<<"No arguments given.\nUse: todo <flag> <item name>\n";
	}
}