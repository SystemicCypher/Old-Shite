#include <iostream>
#include <vector>
#include <fstream>


class Task{
public:
	Task(std::string name = "",std::string description = "", int dueDay = 0, int dueMonth = 0, std::string forWhat = "", bool urgent = false){
		this->name = name;
		this->description = description;
		this->dueDay = dueDay;
		this->dueMonth = dueMonth;
		this->forWhat = forWhat;
		this->urgent = urgent;
	}
	
	void print(){
		std::cout<<name<<"\nDue On: "<<dueMonth<<"/"<<dueDay<<"\nFor: "<<forWhat<<"\nDescription: "<<description<<"\n";
	}

	std::string getName(){
		return name;
	}


private:
	std::string name;
	std::string description;
	int dueDay, dueMonth;
	std::string forWhat;
	bool urgent;
};



