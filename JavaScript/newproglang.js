function ScratchLexer(text) {
    var words = text.split(/\s+/);
    var next = 0;
    this.nextWord = function () {
        if (next >= words.length) return null;
        return words[next++];
    };
}

function Scratch () {
    var dictionary = {};

    this.stack = [];

    this.addWords = function (new_dict) {
        for (var word in new_dict)
            dictionary[word.toUpperCase()] = new_dict[word];
    };

    this.run = function (text) {
        var lexer = new ScratchLexer(text);
        var word;
        var num_val;

        while (word = lexer.nextWord()) {
            word = word.toUpperCase();
            num_val = parseFloat(word);
            if (dictionary[word]) {
                dictionary[word](this);
            } else if (!isNaN(num_val)) {
                this.stack.push(num_val);
            } else {
                throw "Unknown word";
            }
        }
    };
}

/*var terp = new Scratch(); // 'terp is short for interpreter, btw...
terp.run("1 2 3 45 678");
alert(terp.stack);
*/
var PrintingWords = {
    // Print and discard top of stack.
    "PRINT": function (terp) {
        if (terp.stack.length < 1) throw "Not enough items on stack";
        var tos = terp.stack.pop(); alert(tos);
    },
    // Print out the contents of the stack.
    "PSTACK": function (terp) {
        alert(terp.stack);
    }
};

var MathWords = {
    "+": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        terp.stack.push(_2os + tos);
    },
    "-": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        terp.stack.push(_2os - tos);
    },
    "*": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        terp.stack.push(_2os * tos);
    },
    "/": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        terp.stack.push(_2os / tos);
    },
    "SQRT": function (terp) {
        if (terp.stack.length < 1) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        terp.stack.push(Math.sqrt(tos));
    }
};
/*
terp.addWords(PrintingWords);
*/

var StackWords = {
    // Duplicate the top of stack (TOS).
    "DUP": function (terp) {
        if (terp.stack.length < 1) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        terp.stack.push(tos);
        terp.stack.push(tos);
    },
    // Throw away the TOS -- the opposite of DUP.
    "DROP": function (terp) {
        if (terp.stack.length < 1) throw "Not enough items on stack";
        terp.stack.pop();
    },
    // Exchange positions of TOS and second item on stack (2OS).
    "SWAP": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        terp.stack.push(tos);
        terp.stack.push(_2os);
    },
    // Copy 2OS on top of stack.
    "OVER": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        terp.stack.push(_2os);
        terp.stack.push(tos);
        terp.stack.push(_2os);
    },
    // Bring the 3rd item on stack to the top.
    "ROT": function (terp) {
        if (terp.stack.length < 3) throw "Not enough items on stack";
        var tos = terp.stack.pop();
        var _2os = terp.stack.pop();
        var _3os = terp.stack.pop();
        terp.stack.push(_2os);
        terp.stack.push(tos);
        terp.stack.push(_3os);
    },
};

function makeVariable(terp) {
    var me = {value: 0};
    return function () { terp.stack.push(me); };
}

var VariableWords = {
    // Read next word from input and make it a variable.
    "VAR": function (terp) {
        var var_name = terp.lexer.nextWord();
        if (var_name == null) throw "Unexpected end of input";

        terp.define(var_name, makeVariable(terp));
    },
	// Store value of 2OS into variable given by TOS.
    "STORE": function (terp) {
        if (terp.stack.length < 2) throw "Not enough items on stack";
        var reference = terp.stack.pop();
        var new_value = terp.stack.pop();
        reference.value = new_value;
    },
    // Replace reference to variable on TOS with its value.
    "FETCH": function (terp) {
        if (terp.stack.length < 1) throw "Not enough items on stack";
        var reference = terp.stack.pop();
        terp.push(reference.value);

};

    // This goes into the definition of Scratch()
    this.define = function (word, code) {
        dictionary[word.toUpperCase()] = code;
    }